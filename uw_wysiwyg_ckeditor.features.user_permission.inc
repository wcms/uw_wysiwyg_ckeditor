<?php

/**
 * @file
 * uw_wysiwyg_ckeditor.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_wysiwyg_ckeditor_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'access ckeditor link'.
  $permissions['access ckeditor link'] = array(
    'name' => 'access ckeditor link',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'events editor' => 'events editor',
      'form editor' => 'form editor',
      'site manager' => 'site manager',
    ),
    'module' => 'ckeditor_link',
  );

  // Exported permission: 'administer ckeditor link'.
  $permissions['administer ckeditor link'] = array(
    'name' => 'administer ckeditor link',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'ckeditor_link',
  );

  // Exported permission: 'show format selection for comment'.
  $permissions['show format selection for comment'] = array(
    'name' => 'show format selection for comment',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'better_formats',
  );

  // Exported permission: 'show format selection for node'.
  $permissions['show format selection for node'] = array(
    'name' => 'show format selection for node',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'better_formats',
  );

  // Exported permission: 'show format selection for taxonomy_term'.
  $permissions['show format selection for taxonomy_term'] = array(
    'name' => 'show format selection for taxonomy_term',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'better_formats',
  );

  // Exported permission: 'show format selection for user'.
  $permissions['show format selection for user'] = array(
    'name' => 'show format selection for user',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'better_formats',
  );

  // Exported permission: 'show format tips'.
  $permissions['show format tips'] = array(
    'name' => 'show format tips',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'better_formats',
  );

  // Exported permission: 'show more format tips link'.
  $permissions['show more format tips link'] = array(
    'name' => 'show more format tips link',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'better_formats',
  );

  // Exported permission: 'use text format full_html'.
  $permissions['use text format full_html'] = array(
    'name' => 'use text format full_html',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'filter',
  );

  // Exported permission: 'use text format single_page_remote_events'.
  $permissions['use text format single_page_remote_events'] = array(
    'name' => 'use text format single_page_remote_events',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'filter',
  );

  // Exported permission: 'use text format uw_tf_basic'.
  $permissions['use text format uw_tf_basic'] = array(
    'name' => 'use text format uw_tf_basic',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
      'award content author' => 'award content author',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'events editor' => 'events editor',
      'form editor' => 'form editor',
      'site manager' => 'site manager',
      'special alerter' => 'special alerter',
    ),
    'module' => 'filter',
  );

  // Exported permission: 'use text format uw_tf_comment'.
  $permissions['use text format uw_tf_comment'] = array(
    'name' => 'use text format uw_tf_comment',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
      'award content author' => 'award content author',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'form editor' => 'form editor',
      'form results access' => 'form results access',
      'site manager' => 'site manager',
      'special alerter' => 'special alerter',
      'uWaterloo faculty' => 'uWaterloo faculty',
      'uWaterloo staff' => 'uWaterloo staff',
      'uWaterloo student' => 'uWaterloo student',
    ),
    'module' => 'filter',
  );

  // Exported permission: 'use text format uw_tf_contact'.
  $permissions['use text format uw_tf_contact'] = array(
    'name' => 'use text format uw_tf_contact',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'events editor' => 'events editor',
      'site manager' => 'site manager',
    ),
    'module' => 'filter',
  );

  // Exported permission: 'use text format uw_tf_standard'.
  $permissions['use text format uw_tf_standard'] = array(
    'name' => 'use text format uw_tf_standard',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'award content author' => 'award content author',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'events editor' => 'events editor',
      'form editor' => 'form editor',
      'site manager' => 'site manager',
      'special alerter' => 'special alerter',
    ),
    'module' => 'filter',
  );

  // Exported permission: 'use text format uw_tf_standard_sidebar'.
  $permissions['use text format uw_tf_standard_sidebar'] = array(
    'name' => 'use text format uw_tf_standard_sidebar',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'events editor' => 'events editor',
      'form editor' => 'form editor',
      'site manager' => 'site manager',
    ),
    'module' => 'filter',
  );

  // Exported permission: 'use text format uw_tf_standard_site_footer'.
  $permissions['use text format uw_tf_standard_site_footer'] = array(
    'name' => 'use text format uw_tf_standard_site_footer',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'events editor' => 'events editor',
      'site manager' => 'site manager',
    ),
    'module' => 'filter',
  );

  // Exported permission: 'use text format uw_tf_standard_wide'.
  $permissions['use text format uw_tf_standard_wide'] = array(
    'name' => 'use text format uw_tf_standard_wide',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'award content author' => 'award content author',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'events editor' => 'events editor',
      'form editor' => 'form editor',
      'site manager' => 'site manager',
      'special alerter' => 'special alerter',
    ),
    'module' => 'filter',
  );

  return $permissions;
}
