<?php

/**
 * @file
 * uw_wysiwyg_ckeditor.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function uw_wysiwyg_ckeditor_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'ckeditor_link_args_field';
  $strongarm->value = 1;
  $export['ckeditor_link_args_field'] = $strongarm;

  return $export;
}
