/**
 * @file
 * Register a template definition set named "default".
 */

CKEDITOR.addTemplates('default',
{
  // The name of the subfolder that contains the preview images of the templates.
  // imagesPath : CKEDITOR.getUrl( CKEDITOR.plugins.getPath( 'templates' ) + 'templates/images/' ),.
  // Template definitions.
  // Empty paragraphs inside DIVs force any text typed to be a paragraph by default.
  templates :
        [
            {
              title: '3 columns',
              description: '360px | 360px | 220px',
              html:
              '<div class="fcol three-a-1"><p></p></div>' +
              '<div class="fcol three-a-2"><p></p></div>' +
              '<div class="fcol three-a-3"><p></p></div>'
            },
            {
              title: '3 columns',
              description: '210px | 365px | 365px',
              html:
              '<div class="fcol three-1"><p></p></div>' +
              '<div class="fcol three-2"><p></p></div>' +
              '<div class="fcol three-3"><p></p></div>'
            },
            {
              title: '4 columns',
              description: '210px | 245px | 245px | 220px',
              html:
              '<div class="fcol four-1"><p></p></div>' +
              '<div class="fcol four-2"><p></p></div>' +
              '<div class="fcol four-3"><p></p></div>' +
              '<div class="fcol four-4"><p></p></div>'
            },
            {
              title: '5 columns',
              description: '210px | 170px | 170px | 170px | 180px',
              html:
              '<div class="fcol five-1"><p></p></div>' +
              '<div class="fcol five-2"><p></p></div>' +
              '<div class="fcol five-3"><p></p></div>' +
              '<div class="fcol five-4"><p></p></div>' +
              '<div class="fcol five-5"><p></p></div>'
            }
        ]
});
