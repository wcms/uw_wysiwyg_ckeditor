/**
 * @file
 * Register a template definition set named "default".
 */

CKEDITOR.addTemplates('default',
{
  // The name of the subfolder that contains the preview images of the templates.
  // imagesPath : CKEDITOR.getUrl( CKEDITOR.plugins.getPath( 'templates' ) + 'templates/images/' ),.
  // Template definitions.
  // Empty paragraphs inside DIVs force any text typed to be a paragraph by default.
  templates :
        [
            {
              title: '2 columns, 50/50',
              description: 'Columns are 240px wide<br />(360px on wide pages)',
              html:
              '<div class="col-50 first"><p></p></div>' +
              '<div class="col-50"><p></p></div>' +
              '<div class="clearfix"></div>'
            },
            {
              title: '2 columns, 33/66',
              description: 'Columns are 160px | 320px wide<br />(240px | 480px on wide pages)',
              html:
              '<div class="col-33 first"><p></p></div>' +
              '<div class="col-66"><p></p></div>' +
              '<div class="clearfix"></div>'
            },
            {
              title: '2 columns, 66/33',
              description: 'Columns are 320px | 160px wide<br />(480px | 240px on wide pages)',
              html:
              '<div class="col-66 first"><p></p></div>' +
              '<div class="col-33"><p></p></div>' +
              '<div class="clearfix"></div>'
            },
            {
              title: '3 columns, 33/33/33',
              description: 'Columns are 153px wide<br />(230px on wide pages)',
              html:
              '<div class="threecol-33"><p></p></div>' +
              '<div class="threecol-33"><p></p></div>' +
              '<div class="threecol-33 last"><p></p></div>' +
              '<div class="clearfix"></div>'
            }
        ]
});
