/**
 * @file
 * Register a template definition set named "default".
 */

CKEDITOR.addTemplates('default',
{
  // The name of the subfolder that contains the preview images of the templates.
  // imagesPath : CKEDITOR.getUrl( CKEDITOR.plugins.getPath( 'templates' ) + 'templates/images/' ),.
  // Template definitions.
  // Empty paragraphs inside DIVs force any text typed to be a paragraph by default.
  templates :
        [
            {
              title: '2 columns, 50/50',
              description: 'Columns are 240px wide<br />(360px on wide pages)',
              html:
              '<div class="col-50 first"><p></p></div>' +
              '<div class="col-50"><p></p></div>' +
              '<div class="clearfix"></div>'
            },
            {
              title: '2 columns, 33/66',
              description: 'Columns are 160px | 320px wide<br />(240px | 480px on wide pages)',
              html:
              '<div class="col-33 first"><p></p></div>' +
              '<div class="col-66"><p></p></div>' +
              '<div class="clearfix"></div>'
            },
            {
              title: '2 columns, 66/33',
              description: 'Columns are 320px | 160px wide<br />(480px | 240px on wide pages)',
              html:
              '<div class="col-66 first"><p></p></div>' +
              '<div class="col-33"><p></p></div>' +
              '<div class="clearfix"></div>'
            },
            {
              title: '3 columns, 33/33/33',
              description: 'Columns are 153px wide<br />(230px on wide pages)',
              html:
              '<div class="threecol-33"><p></p></div>' +
              '<div class="threecol-33"><p></p></div>' +
              '<div class="threecol-33 last"><p></p></div>' +
              '<div class="clearfix"></div>'
            },
            {
              title: 'Expandable/collapsable content',
              description: 'Use with caution; improper use will cause usability issues.<br /><br />Not available for the "special alert" content type or for "web form" markup elements. Limit use in "image galleries" to only 1 body area. Will show as expanded content on listing pages.',
              html:
              '<div class="expandable"><h2>Expandable title</h2>' +
              '<div class="expandable-content"><p>Expandable text.</p></div>' +
              '</div>'
            },
            {
                title: 'Block Quotes',
                description: 'To be used as a blockquote with citation.',
                html:
                    '<blockquote>' +
                    '<p>We should never forget how lucky we were to have men like Professor Tutte in our darkest hour and the extent to which their work not only helped protect Britain itself but [saved] countless lives.</p>' +
                    '<footer>' +
                    '<cite>' +
                    '<strong>David Cameron</strong>, British Prime Minister, in a 2012 letter to Tutte’s remaining family in Newmarket, England' +
                    '</cite>' +
                    '</footer>' +
                    '</blockquote>'
            }
        ]
});
