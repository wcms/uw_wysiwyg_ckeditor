/**
 * @file
 * UWaterloo CKEditor configuration.
 */

/*jslint browser: true, sloppy: true, vars: true, regexp: true */
/*global CKEDITOR, Drupal */

(function () {
  CKEDITOR.plugins.add('uw_config', {
    init: function (editor) {
      // These filters run when content is pasted and on return from source view.
      editor.dataProcessor.dataFilter.addRules({
        elements : {
          $ : function (element) {
            delete element.attributes.style;
          },
          h1 : function (element) {
            element.name = 'h2';
          }
        }
      });

      // These filters modify the text output of the editor when it quits.
      // Array syntax required; 'break' is a predefined token in JavaScript.
      // Make sure this is defined before trying to change the definition.
      if (Drupal.wysiwyg.plugins['break']) {
        Drupal.wysiwyg.plugins['break'].detach = function (content) {
          return content.replace(/<img [^>]* class="[^">]*wysiwyg-break[^">]*" [^>]*>/, '<!--break-->');
        };
      }

      // These filters modify the DOM before it is serialized for saving.
      editor.dataProcessor.htmlFilter.addRules({
        elements : {
          $ : function (element) {
            // Output dimensions of images as @width and @height instead of @style.
            if (element.name === 'img') {
              var style = element.attributes.style;

              if (style) {
                // Get the width from the style.
                var match = /(?:^|\s)width\s*:\s*(\d+)px/i.exec(style),
                  width = match && match[1];

                // Get the height from the style.
                match = /(?:^|\s)height\s*:\s*(\d+)px/i.exec(style);
                var height = match && match[1];

                if (width) {
                  element.attributes.style = element.attributes.style.replace(/(?:^|\s)width\s*:\s*(\d+)px;?/i, '');
                  element.attributes.width = width;
                }

                if (height) {
                  element.attributes.style = element.attributes.style.replace(/(?:^|\s)height\s*:\s*(\d+)px;?/i, '');
                  element.attributes.height = height;
                }
              }
            }

            // Never emit @style.
            delete element.attributes.style;

            // Delete empty attributes.
            if (!element.attributes.title) {
              delete element.attributes.title;
            }

            return element;
          }
        }
      });
    }
  });

  // Alter displayed text strings.
  CKEDITOR.lang.en.table.caption += ' (displayed to all users)';
  CKEDITOR.lang.en.table.summary += ' (description of table structure read to screen reader users)';

  // Alter dialog boxes, removing undesired controls.
  CKEDITOR.on('dialogDefinition', function (ev) {
    var dialogName = ev.data.name;
    var dialogDefinition = ev.data.definition;
    var tab;
    if (dialogName === 'table' || dialogName === 'tableProperties') {
      // UI dialog defined in plugins/table/dialogs/table.js.
      tab = dialogDefinition.getContents('info');
      tab.remove('txtBorder');
      tab.remove('cmbAlign');
      tab.remove('txtWidth');
      tab.remove('cmbWidthType');
      tab.remove('txtHeight');
      tab.remove('htmlHeightType');
      tab.remove('txtCellSpace');
      tab.remove('txtCellPad');
      tab.get('selHeaders')['default'] = 'row';
    }
    else if (dialogName === 'cellProperties') {
      // UI dialog defined in plugins/tabletools/dialogs/tableCell.js.
      tab = dialogDefinition.getContents('info');
      tab.remove('wordWrap');
      tab.remove('bgColor');
      tab.remove('bgColorChoose');
      tab.remove('borderColor');
      tab.remove('borderColorChoose');
      tab.remove('vAlign');
      tab.remove('hAlign');
      tab.remove('width');
      tab.remove('height');
      tab.remove('widthType');
      tab.remove('htmlHeightType');
    }
    else if (dialogName === 'link') {
      dialogDefinition.removeContents('target');
      tab = dialogDefinition.getContents('advanced');
      tab.remove('advCharset');
      tab.remove('advTabIndex');
      tab.remove('advAccessKey');
      tab.remove('advName');
      var oldOk = dialogDefinition.onOk;
      dialogDefinition.onOk = function () {
        if ((this.getValueOf('info', 'linkType') == 'drupal') && !this._.selectedElement) {
          var drupal_path = this.getValueOf('info', 'drupal_path');
          if (drupal_path.match("(?! )[0-9a-zA-Z-\/]*$")) {
            oldOk.call(this);
          }
          else {
            alert('Link must be a valid internal path.');
            oldOk.call(false);
          }
        }
        else {
          oldOk.call(this);
        }
      };

    }
    else if (dialogName === 'anchor') {
      var oldOk = dialogDefinition.onOk;
      dialogDefinition.onOk = function () {
        var anchor_path = this.getValueOf('info', 'txtName');
        if (anchor_path.match("^[a-zA-z][0-9a-zA-Z-]*$")) {
          oldOk.call(this);
        }
        else {
          alert('Anchor must start with a letter and contain only letters, numbers and dashes.');
          oldOk.call(false);
        }
      };

    }
    else if (dialogName === 'image') {
      tab = dialogDefinition.getContents('info');
      tab.remove('txtBorder');
      tab.remove('txtHSpace');
      tab.remove('txtVSpace');
      tab.remove('cmbAlign');

      var txtAlt = tab.get('txtAlt');
      txtAlt.required = true;
      txtAlt.validate = CKEDITOR.dialog.validate.notEmpty('Alternative text must be provided.');

      tab = dialogDefinition.getContents('Link');
      tab.remove('cmbTarget');

      tab = dialogDefinition.getContents('advanced');
      tab.remove('cmbLangDir');
      tab.remove('txtdlgGenStyle');
    }
    else if (dialogName === 'editdiv' || dialogName === 'creatediv') {
      tab = dialogDefinition.getContents('advanced');
      tab.remove('style');
    }

    // Alter all Advanced tabs.
    tab = dialogDefinition.getContents('advanced');
    if (tab) {
      // Language direction.
      tab.remove('advLangDir');
      // Inline style.
      tab.remove('advStyles');
    }
  });

  CKEDITOR.on('instanceReady', function (ev) {
    // Remove all images from pasted content.
    ev.editor.on('paste', function (ev) {
      // Do the right operation for the right data.
      if (ev.data.html) {
        ev.data.html = ev.data.html.replace(/<img( [^>]*)?>/gi, '');
      }
      if (ev.data.text) {
        ev.data.text = ev.data.text.replace(/<img( [^>]*)?>/gi, '');
      }
    });

    // Prevent drag-and-drop from outside CKEditor, but allow it from inside.
    // By default, assume drags start outside the editor.
    // Make dragstart_outside a global variable to allow dragging between instances of the editor.
    var dragstart_outside = true;
    // When a drag starts inside the editor, thus firing dragstart, record this in dragstart_outside.
    ev.editor.document.on('dragstart', function (ev) {
      dragstart_outside = false;
    });
    // Prevent drops that start outside, reset dragstart_outside.
    ev.editor.document.on('drop', function (ev) {
      if (dragstart_outside) {
        ev.data.preventDefault(true);
      }
      dragstart_outside = true;
    });

    // Remove empty p and h# tags.
    ev.editor.on('getData', function (ev) {
      ev.data.dataValue = ev.data.dataValue.replace(/<(p|h[1-6])>&nbsp;<\/(p|h[1-6])>/gi, '');
    });

    // When we change modes (to/from source view), call getData to strip out empty p and h# tags
    // this breaks some things - maybe a different event? Event Summary @ http://docs.cksource.com/ckeditor_api/symbols/CKEDITOR.editor.html
    ev.editor.on('mode', function (ev) {
      // ev.editor.setData(ev.editor.getData());
    });

    ev.editor.document.on('DOMNodeInserted', function (ev) {
      var target = ev.data.getTarget();
      if (target.is && target.is('img') && target.getAttribute('src') && target.getAttribute('src').substr(0, 5) === 'data:' && !target.getAttribute('data-cke-real-element-type')) {
        target.remove();
      }
    });

  });
}());
