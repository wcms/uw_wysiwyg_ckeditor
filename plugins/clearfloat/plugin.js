/**
 * @file
 * Custom uWaterloo CKEditor button for inserting a float clear.
 */

(function () {
  // Set up the command to be called when the buttons are clicked.
  var clearfloatCommand = {
    exec:function (editor) {
      /*
      //editor will automatically put an &nbsp; inside the div
      //if we don't add the paragraph, editor will put the cursor inside the div, which we don't want
      editor.insertHtml('<div class="clearfix"></div><p></p>');
       */
      var element = editor.getSelection().getStartElement();
      var insert = new CKEDITOR.dom.element('div');
      insert.addClass('clearfix');
      insert.insertBefore(element);
    }
  }

  CKEDITOR.plugins.add('clearfloat', {
    init: function (editor) {

      // Register command.
      var clearfloat = editor.addCommand('clearfloat', clearfloatCommand);

      // Register buttons - which run commands.
      editor.ui.addButton('Clear Floats', {
        label: 'Insert Float Clear',
        command: 'clearfloat',
        icon: this.path + 'float.png'
      });

    }

  });
}());
