/**
 * @file
 * Custom uWaterloo CKEditor buttons for aligning images.
 */

(function () {

  function onSelectionChange(evt) {

    if (evt.editor.readOnly) {
      return;
    }

    var selection = evt.editor.getSelection().getSelectedElement();
    // Selecting a flash object in editor mode still returns a tag of 'img' (this is because an embedded object has a fake parser element).
    // We only want to work with real elements so we check for the 'data-cke-realelement' attribute (which could represent a fake parser element).
    if (selection && selection.is('img') && !selection.hasAttribute('data-cke-realelement')) {
      if (selection.hasClass(this.name)) {
        return this.setState(CKEDITOR.TRISTATE_ON);
      }
      return this.setState(CKEDITOR.TRISTATE_OFF);
    }
    else {
        return this.setState(CKEDITOR.TRISTATE_DISABLED);
    }
  }

  function imagealignCommand(editor, name) {
    this.name = name;
  }

  imagealignCommand.prototype = {
    exec: function (editor) {

      var element = editor.getSelection().getSelectedElement();

      if (!element) {
        return;
      }

      element.removeClass('image-left');
      element.removeClass('image-right');
      element.removeClass('image-center');

      if (this.state === CKEDITOR.TRISTATE_OFF) {
        element.addClass(this.name);
      }

      editor.focus();
      editor.forceNextSelectionCheck();

    }
  };

  CKEDITOR.plugins.add('imagealign', {
    init: function (editor) {

      // Register commands.
      var left = editor.addCommand('image-left', new imagealignCommand(editor, 'image-left'));
      left.startDisabled = true;
      var center = editor.addCommand('image-center', new imagealignCommand(editor, 'image-center'));
      center.startDisabled = true;
      var right = editor.addCommand('image-right', new imagealignCommand(editor, 'image-right'));
      right.startDisabled = true;

      // Register buttons.
      editor.ui.addButton('Image Left', {
        label: 'Align Image Left',
        command: 'image-left',
        icon: this.path + "image-left.png"
      });

      editor.ui.addButton('Image Center', {
        label: 'Center Image',
        command: 'image-center',
        icon: this.path + "image-center.png"
      });

      editor.ui.addButton('Image Right', {
        label: 'Align Image Right',
        command: 'image-right',
        icon: this.path + "image-right.png"
      });

      // Register state changing handlers.
      editor.on('selectionChange', CKEDITOR.tools.bind(onSelectionChange, left));
      editor.on('selectionChange', CKEDITOR.tools.bind(onSelectionChange, center));
      editor.on('selectionChange', CKEDITOR.tools.bind(onSelectionChange, right));
    }

  });

}());
