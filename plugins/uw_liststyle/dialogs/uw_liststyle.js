/*
 * Copyright (c) 2003-2012, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

(function()
{
  function getListElement( editor, listTag )
  {
    var range;
    try { range  = editor.getSelection().getRanges()[ 0 ]; }
    catch( e ) { return null; }

    range.shrink( CKEDITOR.SHRINK_TEXT );
    return range.getCommonAncestor().getAscendant( listTag, 1 );
  }

  var listItem = function( node ) { return node.type == CKEDITOR.NODE_ELEMENT && node.is( 'li' ); };

  var mapListStyle = {
    'a' : 'lower-alpha',
    'A' : 'upper-alpha',
    'i' : 'lower-roman',
    'I' : 'upper-roman',
    '1' : 'decimal',
    'disc' : 'disc',
    'circle': 'circle',
    'square' : 'square'
  };

  function listStyle( editor, startupPage )
  {
    var lang = editor.lang.list;
    if ( startupPage == 'bulletedListStyle' )
    {
      return {
        title : 'Bulleted List Properties',
        minWidth : 300,
        minHeight : 50,
        contents :
        [
          {
            id : 'info',
            accessKey : 'I',
            elements :
            [
              {
                type : 'select',
                label : 'Type',
                id : 'type',
                align : 'center',
                style : 'width:150px',
                items :
                [
                  [ '<not set>', '' ],
                  [ 'circle', 'circle' ],
                  [ 'disc',  'disc' ],
                  [ 'square', 'square' ]
                ],
                setup : function( element )
                {
                  var value = element.getStyle( 'list-style-type' )
                        || mapListStyle[ element.getAttribute( 'type' ) ]
                        || element.getAttribute( 'type' )
                        || '';

                  //look for CSS setting
                  if (element.hasClass('circle')) {
                    value = 'circle';
                  } else if (element.hasClass('disc')) {
                    value = 'disc';
                  } else if (element.hasClass('square')){
                    value = 'square';
                  }

                  this.setValue( value );
                },
                commit : function( element )
                {
                  var value = this.getValue();
                  element.removeClass('circle');
                  element.removeClass('disc');
                  element.removeClass('square');
                  if ( value )
                    element.addClass( value );
                }
              }
            ]
          }
        ],
        onShow: function()
        {
          var editor = this.getParentEditor(),
            element = getListElement( editor, 'ul' );

          element && this.setupContent( element );
        },
        onOk: function()
        {
          var editor = this.getParentEditor(),
            element = getListElement( editor, 'ul' );

          element && this.commitContent( element );
        }
      };
    }
    else if ( startupPage == 'numberedListStyle'  )
    {

      var listStyleOptions =
      [
        [ '<not set>', '' ],
        [ 'lower-roman', 'lower-roman' ],
        [ 'upper-roman', 'upper-roman' ],
        [ 'lower-alpha', 'lower-alpha' ],
        [ 'upper-alpha', 'upper-alpha' ],
        [ 'decimal', 'decimal' ]
      ];

      if ( !CKEDITOR.env.ie || CKEDITOR.env.version > 7 )
      {
        listStyleOptions.concat( [
          [ 'armenian', 'armenian' ],
          [ 'decimal-leading-zero', 'decimal-leading-zero' ],
          [ 'georgian', 'georgian' ],
          [ 'lower-greek', 'lower-greek' ]
        ]);
      }

      return {
        title : 'Numbered List Properties',
        minWidth : 300,
        minHeight : 50,
        contents :
        [
          {
            id : 'info',
            accessKey : 'I',
            elements :
            [
              {
                type : 'hbox',
                widths : [ '25%', '75%' ],
                children :
                [
                  {
                    label : 'Start',
                    type : 'text',
                    id : 'start',
                    validate : CKEDITOR.dialog.validate.integer( lang.validateStartNumber ),
                    setup : function( element )
                    {
                      // List item start number dominates.
                      var value = element.getFirst( listItem ).getAttribute( 'value' ) || element.getAttribute( 'start' ) || 1;
                      value && this.setValue( value );
                    },
                    commit : function( element )
                    {
                      var firstItem = element.getFirst( listItem );
                      var oldStart = firstItem.getAttribute( 'value' ) || element.getAttribute( 'start' ) || 1;

                      // Force start number on list root.
                      element.getFirst( listItem ).removeAttribute( 'value' );
                      var val = parseInt( this.getValue(), 10 );
                      if ( isNaN( val ) )
                        element.removeAttribute( 'start' );
                      else
                        element.setAttribute( 'start', val );

                      // Update consequent list item numbering.
                      var nextItem = firstItem, conseq = oldStart, startNumber = isNaN( val ) ? 1 : val;
                      while ( ( nextItem = nextItem.getNext( listItem ) ) && conseq++ )
                      {
                        if ( nextItem.getAttribute( 'value' ) == conseq )
                          nextItem.setAttribute( 'value', startNumber + conseq - oldStart );
                      }
                    }
                  },
                  {
                    type : 'select',
                    label : 'Type',
                    id : 'type',
                    style : 'width: 100%;',
                    items : listStyleOptions,
                    setup : function( element )
                    {
                      var value = element.getStyle( 'list-style-type' )
                        || mapListStyle[ element.getAttribute( 'type' ) ]
                        || element.getAttribute( 'type' )
                        || '';

                      //look for CSS setting
                      if (element.hasClass('lower-roman')) {
                        value = 'lower-roman';
                      } else if (element.hasClass('upper-roman')) {
                        value = 'upper-roman';
                      } else if (element.hasClass('lower-alpha')){
                        value = 'lower-alpha';
                      } else if (element.hasClass('upper-alpha')){
                        value = 'upper-alpha';
                      } else if (element.hasClass('decimal')){
                        value = 'decimal';
                      }

                      this.setValue( value );
                    },
                    commit : function( element )
                    {
                      var value = this.getValue();
                      element.removeClass('lower-roman');
                      element.removeClass('upper-roman');
                      element.removeClass('lower-alpha');
                      element.removeClass('upper-alpha');
                      element.removeClass('decimal');
                      if ( value )
                        element.addClass( value );
                    }
                  }
                ]
              }
            ]
          }
        ],
        onShow: function()
        {
          var editor = this.getParentEditor(),
            element = getListElement( editor, 'ol' );

          element && this.setupContent( element );
        },
        onOk: function()
        {
          var editor = this.getParentEditor(),
            element = getListElement( editor, 'ol' );

          element && this.commitContent( element );
        }
      };
    }
  }

  CKEDITOR.dialog.add( 'numberedListStyle', function( editor )
    {
      return listStyle( editor, 'numberedListStyle' );
    });

  CKEDITOR.dialog.add( 'bulletedListStyle', function( editor )
    {
      return listStyle( editor, 'bulletedListStyle' );
    });
})();
