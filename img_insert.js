/**
 * @file
 * Despite the filename, this affects ALL inserts (i.e. file and image).
 */

(function ($) {
  "use strict";
  if (Drupal.behaviors.insert) {
    // Prevent form submit from being triggered by hitting enter in input elements.
    $(document).ready(function () {
      $('input').keypress(function (e) {
        if (e.which === 13) {
          return false;
        }
      });
    });

    // New version of Drupal.insert.insertIntoActiveEditor which overrides insert.js.
    // Prohibit inserting into a caption textarea.
    Drupal.insert.insertIntoActiveEditor = function (content) {
      var editorElement;

      // WYSIWYG support, should work in all editors if available.
      if (Drupal.wysiwyg && Drupal.wysiwyg.activeId && Drupal.wysiwyg.activeId.indexOf('-caption-') === -1) {
        editorElement = document.getElementById(Drupal.wysiwyg.activeId);
        Drupal.insert.activateTabPane(editorElement);
        Drupal.wysiwyg.instances[Drupal.wysiwyg.activeId].insert(content);
      }

      if (editorElement) {
        Drupal.insert.contentWarning(editorElement, content);
      }

      return false;
    };

    // Respond to "$.event.trigger('insertIntoActiveEditor'..." in insert.js.
    // Prompt for alt text.
    $(document).bind('insertIntoActiveEditor', function (event, options) {
      var altRegExp, alt;
      altRegExp = new RegExp('__alt__', 'g');
      // Only change the content if it contains an __alt__.
      if (options.content.match(altRegExp)) {
        alt = window.prompt('Enter the alternative text to be displayed to users who cannot see the image.');
        // If no alt text provided, empty the content, so that the insert fails.
        if (alt) {
          // Encode entities.
          alt = $('<div/>').text(alt).html().replace(/"/g, '&quot;');
          options.content = options.content.replace(altRegExp, alt);
        }
        else {
          options.content = '';
        }
      }
    });
  }
}(jQuery));
