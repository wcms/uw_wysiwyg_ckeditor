/**
 * @file
 * Register a template definition set named "default".
 */

CKEDITOR.addTemplates('default',
{
  // The name of the subfolder that contains the preview images of the templates.
  // imagesPath : CKEDITOR.getUrl( CKEDITOR.plugins.getPath( 'templates' ) + 'templates/images/' ),
  //
  /* Template definitions.
  templates :
		[
			{
				title: 'Site Footer - 3 Columns',
				description: 'Layout for site footers, aligning with the global footer',
				html:
					'<div class="fcol three-1"><h2 class="no-border">First column</h2></div>' +
					'<div class="fcol three-2"><h2 class="no-border">Second column</h2></div>' +
					'<div class="fcol three-3"><h2 class="no-border">Third column</h2></div>'
			},
			{
				title: 'Site Footer - 4 Columns',
				description: 'Layout for site footers, aligning with the global footer',
				html:
					'<div class="fcol four-1"><h2 class="no-border">First column</h2></div>' +
					'<div class="fcol four-2"><h2 class="no-border">Second column</h2></div>' +
					'<div class="fcol four-3"><h2 class="no-border">Third column</h2></div>' +
					'<div class="fcol four-4"><h2 class="no-border">Fourth column</h2></div>'
			},
			{
				title: 'Site Footer - 5 Columns',
				description: 'Layout for site footers, aligning with the global footer',
				html:
					'<div class="fcol five-1"><h2 class="no-border">First column</h2></div>' +
					'<div class="fcol five-2"><h2 class="no-border">Second column</h2></div>' +
					'<div class="fcol five-3"><h2 class="no-border">Third column</h2></div>' +
					'<div class="fcol five-4"><h2 class="no-border">Fourth column</h2></div>' +
					'<div class="fcol five-5"><h2 class="no-border">Fifth column</h2></div>'
			}
		]
		// */
});
