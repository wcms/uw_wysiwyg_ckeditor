<?php

/**
 * @file
 * uw_wysiwyg_ckeditor.features.wysiwyg.inc
 */

/**
 * Implements hook_wysiwyg_default_profiles().
 */
function uw_wysiwyg_ckeditor_wysiwyg_default_profiles() {
  $profiles = array();

  // Exported profile: full_html.
  $profiles['full_html'] = array(
    'format' => 'full_html',
    'editor' => 'ckeditor',
    'settings' => array(
      'buttons' => array(
        'default' => array(
          'Bold' => 1,
          'Italic' => 1,
          'Underline' => 1,
          'Strike' => 1,
          'JustifyLeft' => 1,
          'JustifyCenter' => 1,
          'JustifyRight' => 1,
          'JustifyBlock' => 1,
          'BulletedList' => 1,
          'NumberedList' => 1,
          'Outdent' => 1,
          'Indent' => 1,
          'Undo' => 1,
          'Redo' => 1,
          'Link' => 1,
          'Unlink' => 1,
          'Anchor' => 1,
          'Image' => 1,
          'TextColor' => 1,
          'BGColor' => 1,
          'Superscript' => 1,
          'Subscript' => 1,
          'Blockquote' => 1,
          'Source' => 1,
          'HorizontalRule' => 1,
          'ShowBlocks' => 1,
          'RemoveFormat' => 1,
          'SpecialChar' => 1,
          'Format' => 1,
          'Font' => 1,
          'FontSize' => 1,
          'Styles' => 1,
          'Table' => 1,
          'SelectAll' => 1,
          'Find' => 1,
          'Replace' => 1,
          'Flash' => 1,
          'Smiley' => 1,
          'CreateDiv' => 1,
          'Maximize' => 1,
          'Scayt' => 1,
          'About' => 1,
        ),
        'socialmedia' => array(
          'Facebook' => 1,
          'Twitter' => 1,
          'Livestream' => 1,
          'Mailman' => 1,
          'Tableau' => 1,
          'Tint' => 1,
          'Vimeo' => 1,
          'MailChimp' => 1,
        ),
        'embedded_factsfigures' => array(
          'Facts and Figures' => 1,
        ),
        'embedded' => array(
          'Timeline' => 1,
        ),
        'galleryembedded' => array(
          'Imagegallery' => 1,
        ),
        'embeddedMaps' => array(
          'Embedded Maps' => 1,
        ),
        'uw_video_embed' => array(
          'uw_video_embed' => 1,
        ),
        'smallerselection' => array(
          'smallerselection' => 1,
        ),
        'uw_config' => array(
          'uw_config' => 1,
        ),
        'imagealign' => array(
          'Image Left' => 1,
          'Image Center' => 1,
          'Image Right' => 1,
        ),
        'clearfloat' => array(
          'Clear Floats' => 1,
        ),
        'templates' => array(
          'Templates' => 1,
        ),
        'uw_liststyle' => array(
          'uw_liststyle' => 1,
        ),
        'codemirror' => array(
          'codemirror' => 1,
        ),
        'textselection' => array(
          'textselection' => 1,
        ),
        'balloonpanel' => array(
          'Balloonpanel' => 1,
        ),
        'a11ychecker' => array(
          'A11ychecker' => 1,
        ),
        'drupal_path' => array(
          'Link' => 1,
        ),
        'drupal' => array(
          'break' => 1,
        ),
      ),
      'theme' => 'advanced',
      'language' => 'en',
      'toolbarLocation' => 'top',
      'resize_enabled' => 1,
      'default_toolbar_grouping' => 0,
      'simple_source_formatting' => 1,
      'acf_mode' => 0,
      'acf_allowed_content' => '',
      'css_setting' => 'none',
      'css_theme' => 'uw_adminimal_theme',
      'css_path' => '',
      'stylesSet' => '',
      'block_formats' => 'p,address,pre,h2,h3,h4,h5,h6,div',
      'forcePasteAsPlainText' => 0,
      'pasteFromWordPromptCleanup' => 0,
      'pasteFromWordRemoveFontStyles' => 1,
    ),
    'preferences' => array(
      'add_to_summaries' => 1,
      'default' => 1,
      'show_toggle' => 1,
      'user_choose' => 0,
      'version' => '4.16.1.cae20318d4',
    ),
    'name' => 'formatfull_html',
    'rdf_mapping' => array(),
  );

  // Exported profile: single_page_remote_events.
  $profiles['single_page_remote_events'] = array(
    'format' => 'single_page_remote_events',
    'editor' => 'ckeditor',
    'settings' => array(
      'buttons' => array(
        'default' => array(
          'Bold' => 1,
          'Italic' => 1,
          'Strike' => 1,
          'BulletedList' => 1,
          'NumberedList' => 1,
          'Outdent' => 1,
          'Indent' => 1,
          'Undo' => 1,
          'Redo' => 1,
          'Image' => 1,
          'Superscript' => 1,
          'Subscript' => 1,
          'Blockquote' => 1,
          'Source' => 1,
          'HorizontalRule' => 1,
          'RemoveFormat' => 1,
          'Styles' => 1,
          'SelectAll' => 1,
          'Find' => 1,
        ),
        'socialmedia' => array(
          'Livestream' => 1,
        ),
        'imagealign' => array(
          'Image Left' => 1,
          'Image Center' => 1,
          'Image Right' => 1,
        ),
        'uw_liststyle' => array(
          'uw_liststyle' => 1,
        ),
        'textselection' => array(
          'textselection' => 1,
        ),
        'balloonpanel' => array(
          'Balloonpanel' => 1,
        ),
        'a11ychecker' => array(
          'A11ychecker' => 1,
        ),
        'drupal' => array(
          'break' => 1,
        ),
      ),
      'theme' => '',
      'language' => 'en',
      'toolbarLocation' => 'top',
      'resize_enabled' => 1,
      'default_toolbar_grouping' => 0,
      'simple_source_formatting' => 0,
      'acf_mode' => 0,
      'acf_allowed_content' => '',
      'css_setting' => 'theme',
      'css_theme' => 'uw_adminimal_theme',
      'css_path' => '',
      'stylesSet' => '',
      'block_formats' => 'p,address,pre,h2,h3,h4,h5,h6,div',
      'forcePasteAsPlainText' => 0,
      'pasteFromWordPromptCleanup' => 0,
      'pasteFromWordRemoveFontStyles' => 0,
    ),
    'preferences' => array(
      'add_to_summaries' => 1,
      'default' => 1,
      'show_toggle' => 1,
      'user_choose' => 0,
      'version' => '4.16.1.cae20318d4',
    ),
    'name' => 'formatsingle_page_remote_events',
    'rdf_mapping' => array(),
  );

  // Exported profile: uw_tf_basic.
  $profiles['uw_tf_basic'] = array(
    'format' => 'uw_tf_basic',
    'editor' => 'ckeditor',
    'settings' => array(
      'buttons' => array(
        'default' => array(
          'Bold' => 1,
          'Italic' => 1,
          'BulletedList' => 1,
          'NumberedList' => 1,
          'Link' => 1,
          'Unlink' => 1,
          'Source' => 1,
        ),
        'smallerselection' => array(
          'smallerselection' => 1,
        ),
        'uw_config' => array(
          'uw_config' => 1,
        ),
        'uw_liststyle' => array(
          'uw_liststyle' => 1,
        ),
        'codemirror' => array(
          'codemirror' => 1,
        ),
        'textselection' => array(
          'textselection' => 1,
        ),
        'balloonpanel' => array(
          'Balloonpanel' => 1,
        ),
        'a11ychecker' => array(
          'A11ychecker' => 1,
        ),
        'drupal_path' => array(
          'Link' => 1,
        ),
      ),
      'theme' => 'advanced',
      'language' => 'en',
      'toolbarLocation' => 'top',
      'resize_enabled' => 1,
      'default_toolbar_grouping' => 0,
      'simple_source_formatting' => 1,
      'acf_mode' => 0,
      'acf_allowed_content' => '',
      'css_setting' => 'theme',
      'css_theme' => 'uw_adminimal_theme',
      'css_path' => '',
      'stylesSet' => '',
      'block_formats' => 'p,address,pre,h2,h3,h4,h5,h6,div',
      'forcePasteAsPlainText' => 0,
      'pasteFromWordPromptCleanup' => 0,
      'pasteFromWordRemoveFontStyles' => 0,
    ),
    'preferences' => array(
      'add_to_summaries' => 1,
      'default' => 1,
      'show_toggle' => 1,
      'user_choose' => 0,
      'version' => '4.16.1.cae20318d4',
    ),
    'name' => 'formatuw_tf_basic',
    'rdf_mapping' => array(),
  );

  // Exported profile: uw_tf_comment.
  $profiles['uw_tf_comment'] = array(
    'format' => 'uw_tf_comment',
    'editor' => 'ckeditor',
    'settings' => array(
      'buttons' => array(
        'default' => array(
          'Bold' => 1,
          'Italic' => 1,
          'Strike' => 1,
          'BulletedList' => 1,
          'NumberedList' => 1,
          'Outdent' => 1,
          'Indent' => 1,
          'Undo' => 1,
          'Redo' => 1,
          'Link' => 1,
          'Unlink' => 1,
          'Anchor' => 1,
          'Superscript' => 1,
          'Subscript' => 1,
          'Blockquote' => 1,
          'Source' => 1,
          'HorizontalRule' => 1,
          'ShowBlocks' => 1,
          'RemoveFormat' => 1,
          'Styles' => 1,
          'Table' => 1,
          'SelectAll' => 1,
          'Find' => 1,
          'Replace' => 1,
          'Maximize' => 1,
          'Scayt' => 1,
        ),
        'smallerselection' => array(
          'smallerselection' => 1,
        ),
        'uw_config' => array(
          'uw_config' => 1,
        ),
        'uw_liststyle' => array(
          'uw_liststyle' => 1,
        ),
        'codemirror' => array(
          'codemirror' => 1,
        ),
        'textselection' => array(
          'textselection' => 1,
        ),
        'balloonpanel' => array(
          'Balloonpanel' => 1,
        ),
        'a11ychecker' => array(
          'A11ychecker' => 1,
        ),
        'drupal_path' => array(
          'Link' => 1,
        ),
      ),
      'theme' => 'advanced',
      'language' => 'en',
      'toolbarLocation' => 'top',
      'resize_enabled' => 1,
      'default_toolbar_grouping' => 0,
      'simple_source_formatting' => 1,
      'acf_mode' => 0,
      'acf_allowed_content' => '',
      'css_setting' => 'theme',
      'css_theme' => 'uw_adminimal_theme',
      'css_path' => '',
      'stylesSet' => '',
      'block_formats' => 'p,address,pre,h2,h3,h4,h5,h6,div',
      'forcePasteAsPlainText' => 0,
      'pasteFromWordPromptCleanup' => 0,
      'pasteFromWordRemoveFontStyles' => 0,
    ),
    'preferences' => array(
      'add_to_summaries' => 1,
      'default' => 1,
      'show_toggle' => 1,
      'user_choose' => 0,
      'version' => '4.16.1.cae20318d4',
    ),
    'name' => 'formatuw_tf_comment',
    'rdf_mapping' => array(),
  );

  // Exported profile: uw_tf_contact.
  $profiles['uw_tf_contact'] = array(
    'format' => 'uw_tf_contact',
    'editor' => 'ckeditor',
    'settings' => array(
      'buttons' => array(
        'default' => array(
          'Bold' => 1,
          'Italic' => 1,
          'BulletedList' => 1,
          'NumberedList' => 1,
          'Outdent' => 1,
          'Indent' => 1,
          'Undo' => 1,
          'Redo' => 1,
          'Link' => 1,
          'Unlink' => 1,
          'Anchor' => 1,
          'Superscript' => 1,
          'Subscript' => 1,
          'Blockquote' => 1,
          'Source' => 1,
          'HorizontalRule' => 1,
          'ShowBlocks' => 1,
          'RemoveFormat' => 1,
          'Styles' => 1,
          'Table' => 1,
          'SelectAll' => 1,
          'Maximize' => 1,
          'Scayt' => 1,
        ),
        'smallerselection' => array(
          'smallerselection' => 1,
        ),
        'uw_config' => array(
          'uw_config' => 1,
        ),
        'templates' => array(
          'Templates' => 1,
        ),
        'uw_liststyle' => array(
          'uw_liststyle' => 1,
        ),
        'codemirror' => array(
          'codemirror' => 1,
        ),
        'textselection' => array(
          'textselection' => 1,
        ),
        'balloonpanel' => array(
          'Balloonpanel' => 1,
        ),
        'a11ychecker' => array(
          'A11ychecker' => 1,
        ),
        'drupal_path' => array(
          'Link' => 1,
        ),
        'drupal' => array(
          'break' => 1,
        ),
      ),
      'theme' => 'advanced',
      'language' => 'en',
      'toolbarLocation' => 'top',
      'resize_enabled' => 1,
      'default_toolbar_grouping' => 0,
      'simple_source_formatting' => 1,
      'acf_mode' => 0,
      'acf_allowed_content' => '',
      'css_setting' => 'none',
      'css_theme' => 'uw_adminimal_theme',
      'css_path' => '',
      'stylesSet' => '',
      'block_formats' => 'p,address,pre,h2,h3,h4,h5,h6,div',
      'forcePasteAsPlainText' => 0,
      'pasteFromWordPromptCleanup' => 0,
      'pasteFromWordRemoveFontStyles' => 0,
    ),
    'preferences' => array(
      'add_to_summaries' => 1,
      'default' => 1,
      'show_toggle' => 1,
      'user_choose' => 0,
      'version' => '4.16.1.cae20318d4',
    ),
    'name' => 'formatuw_tf_contact',
    'rdf_mapping' => array(),
  );

  // Exported profile: uw_tf_standard.
  $profiles['uw_tf_standard'] = array(
    'format' => 'uw_tf_standard',
    'editor' => 'ckeditor',
    'settings' => array(
      'buttons' => array(
        'default' => array(
          'Bold' => 1,
          'Italic' => 1,
          'Strike' => 1,
          'BulletedList' => 1,
          'NumberedList' => 1,
          'Outdent' => 1,
          'Indent' => 1,
          'Undo' => 1,
          'Redo' => 1,
          'Link' => 1,
          'Unlink' => 1,
          'Anchor' => 1,
          'Superscript' => 1,
          'Subscript' => 1,
          'Blockquote' => 1,
          'Source' => 1,
          'HorizontalRule' => 1,
          'ShowBlocks' => 1,
          'RemoveFormat' => 1,
          'Styles' => 1,
          'Table' => 1,
          'SelectAll' => 1,
          'Find' => 1,
          'Replace' => 1,
          'Maximize' => 1,
          'Scayt' => 1,
        ),
        'socialmedia' => array(
          'Facebook' => 1,
          'Twitter' => 1,
          'Livestream' => 1,
          'Mailman' => 1,
          'Tableau' => 1,
          'Tint' => 1,
          'Vimeo' => 1,
          'MailChimp' => 1,
          'Hootsuite' => 1,
          'CodePen' => 1,
          'Social Intents' => 1,
        ),
        'embedded_calltoaction' => array(
          'Call to Action' => 1,
        ),
        'embedded_factsfigures' => array(
          'Facts and Figures' => 1,
        ),
        'embedded' => array(
          'Timeline' => 1,
        ),
        'galleryembedded' => array(
          'Imagegallery' => 1,
        ),
        'embeddedMaps' => array(
          'Embedded Maps' => 1,
        ),
        'embedded_powerbi' => array(
          'Power BI' => 1,
        ),
        'uw_video_embed' => array(
          'uw_video_embed' => 1,
        ),
        'smallerselection' => array(
          'smallerselection' => 1,
        ),
        'uw_config' => array(
          'uw_config' => 1,
        ),
        'imagealign' => array(
          'Image Left' => 1,
          'Image Center' => 1,
          'Image Right' => 1,
        ),
        'clearfloat' => array(
          'Clear Floats' => 1,
        ),
        'templates' => array(
          'Templates' => 1,
        ),
        'uw_liststyle' => array(
          'uw_liststyle' => 1,
        ),
        'codemirror' => array(
          'codemirror' => 1,
        ),
        'textselection' => array(
          'textselection' => 1,
        ),
        'balloonpanel' => array(
          'Balloonpanel' => 1,
        ),
        'a11ychecker' => array(
          'A11ychecker' => 1,
        ),
        'drupal_path' => array(
          'Link' => 1,
        ),
        'drupal' => array(
          'break' => 1,
        ),
      ),
      'theme' => 'advanced',
      'language' => 'en',
      'toolbarLocation' => 'top',
      'resize_enabled' => 1,
      'default_toolbar_grouping' => 0,
      'simple_source_formatting' => 1,
      'acf_mode' => 0,
      'acf_allowed_content' => '',
      'css_setting' => 'none',
      'css_theme' => 'uw_adminimal_theme',
      'css_path' => '',
      'stylesSet' => '',
      'block_formats' => 'p,address,pre,h2,h3,h4,h5,h6,div',
      'forcePasteAsPlainText' => 0,
      'pasteFromWordPromptCleanup' => 0,
      'pasteFromWordRemoveFontStyles' => 1,
    ),
    'preferences' => array(
      'default' => 1,
      'user_choose' => 0,
      'show_toggle' => 1,
      'add_to_summaries' => 1,
      'version' => '4.16.1.cae20318d4',
    ),
    'name' => 'formatuw_tf_standard',
    'rdf_mapping' => array(),
  );

  // Exported profile: uw_tf_standard_sidebar.
  $profiles['uw_tf_standard_sidebar'] = array(
    'format' => 'uw_tf_standard_sidebar',
    'editor' => 'ckeditor',
    'settings' => array(
      'buttons' => array(
        'default' => array(
          'Bold' => 1,
          'Italic' => 1,
          'Strike' => 1,
          'BulletedList' => 1,
          'NumberedList' => 1,
          'Outdent' => 1,
          'Indent' => 1,
          'Undo' => 1,
          'Redo' => 1,
          'Link' => 1,
          'Unlink' => 1,
          'Anchor' => 1,
          'Superscript' => 1,
          'Subscript' => 1,
          'Blockquote' => 1,
          'Source' => 1,
          'HorizontalRule' => 1,
          'ShowBlocks' => 1,
          'RemoveFormat' => 1,
          'Styles' => 1,
          'Table' => 1,
          'SelectAll' => 1,
          'Find' => 1,
          'Replace' => 1,
          'Maximize' => 1,
          'Scayt' => 1,
        ),
        'socialmedia' => array(
          'Facebook' => 1,
          'Twitter' => 1,
          'Mailman' => 1,
          'MailChimp' => 1,
          'Social Intents' => 1,
        ),
        'embedded_calltoaction' => array(
          'Call to Action' => 1,
        ),
        'embedded_factsfigures' => array(
          'Facts and Figures' => 1,
        ),
        'embedded' => array(
          'Timeline' => 1,
        ),
        'galleryembedded' => array(
          'Imagegallery' => 1,
        ),
        'smallerselection' => array(
          'smallerselection' => 1,
        ),
        'uw_config' => array(
          'uw_config' => 1,
        ),
        'imagealign' => array(
          'Image Left' => 1,
          'Image Center' => 1,
          'Image Right' => 1,
        ),
        'clearfloat' => array(
          'Clear Floats' => 1,
        ),
        'templates' => array(
          'Templates' => 1,
        ),
        'uw_liststyle' => array(
          'uw_liststyle' => 1,
        ),
        'codemirror' => array(
          'codemirror' => 1,
        ),
        'textselection' => array(
          'textselection' => 1,
        ),
        'balloonpanel' => array(
          'Balloonpanel' => 1,
        ),
        'a11ychecker' => array(
          'A11ychecker' => 1,
        ),
        'drupal_path' => array(
          'Link' => 1,
        ),
      ),
      'theme' => 'advanced',
      'language' => 'en',
      'toolbarLocation' => 'top',
      'resize_enabled' => 1,
      'default_toolbar_grouping' => 0,
      'simple_source_formatting' => 1,
      'acf_mode' => 0,
      'acf_allowed_content' => '',
      'css_setting' => 'none',
      'css_theme' => 'uw_adminimal_theme',
      'css_path' => '',
      'stylesSet' => '',
      'block_formats' => 'p,address,pre,h2,h3,h4,h5,h6,div',
      'forcePasteAsPlainText' => 0,
      'pasteFromWordPromptCleanup' => 0,
      'pasteFromWordRemoveFontStyles' => 1,
    ),
    'preferences' => array(
      'default' => 1,
      'user_choose' => 0,
      'show_toggle' => 1,
      'add_to_summaries' => 1,
      'version' => '4.16.1.cae20318d4',
    ),
    'name' => 'formatuw_tf_standard_sidebar',
    'rdf_mapping' => array(),
  );

  // Exported profile: uw_tf_standard_site_footer.
  $profiles['uw_tf_standard_site_footer'] = array(
    'format' => 'uw_tf_standard_site_footer',
    'editor' => 'ckeditor',
    'settings' => array(
      'buttons' => array(
        'default' => array(
          'Bold' => 1,
          'Italic' => 1,
          'Strike' => 1,
          'BulletedList' => 1,
          'NumberedList' => 1,
          'Outdent' => 1,
          'Indent' => 1,
          'Undo' => 1,
          'Redo' => 1,
          'Link' => 1,
          'Unlink' => 1,
          'Anchor' => 1,
          'Superscript' => 1,
          'Subscript' => 1,
          'Blockquote' => 1,
          'Source' => 1,
          'HorizontalRule' => 1,
          'ShowBlocks' => 1,
          'RemoveFormat' => 1,
          'Styles' => 1,
          'Table' => 1,
          'SelectAll' => 1,
          'Find' => 1,
          'Replace' => 1,
          'Maximize' => 1,
          'Scayt' => 1,
        ),
        'socialmedia' => array(
          'Facebook' => 1,
          'Twitter' => 1,
          'Mailman' => 1,
          'MailChimp' => 1,
          'Social Intents' => 1,
        ),
        'smallerselection' => array(
          'smallerselection' => 1,
        ),
        'uw_config' => array(
          'uw_config' => 1,
        ),
        'imagealign' => array(
          'Image Left' => 1,
          'Image Center' => 1,
          'Image Right' => 1,
        ),
        'clearfloat' => array(
          'Clear Floats' => 1,
        ),
        'templates' => array(
          'Templates' => 1,
        ),
        'uw_liststyle' => array(
          'uw_liststyle' => 1,
        ),
        'codemirror' => array(
          'codemirror' => 1,
        ),
        'textselection' => array(
          'textselection' => 1,
        ),
        'balloonpanel' => array(
          'Balloonpanel' => 1,
        ),
        'a11ychecker' => array(
          'A11ychecker' => 1,
        ),
        'drupal_path' => array(
          'Link' => 1,
        ),
      ),
      'theme' => 'advanced',
      'language' => 'en',
      'toolbarLocation' => 'top',
      'resize_enabled' => 1,
      'default_toolbar_grouping' => 0,
      'simple_source_formatting' => 1,
      'acf_mode' => 0,
      'acf_allowed_content' => '',
      'css_setting' => 'none',
      'css_theme' => 'uw_adminimal_theme',
      'css_path' => '',
      'stylesSet' => '',
      'block_formats' => 'p,address,pre,h2,h3,h4,h5,h6,div',
      'forcePasteAsPlainText' => 0,
      'pasteFromWordPromptCleanup' => 0,
      'pasteFromWordRemoveFontStyles' => 0,
    ),
    'preferences' => array(
      'default' => 1,
      'user_choose' => 0,
      'show_toggle' => 1,
      'add_to_summaries' => 1,
      'version' => '4.16.1.cae20318d4',
    ),
    'name' => 'formatuw_tf_standard_site_footer',
    'rdf_mapping' => array(),
  );

  // Exported profile: uw_tf_standard_wide.
  $profiles['uw_tf_standard_wide'] = array(
    'format' => 'uw_tf_standard_wide',
    'editor' => 'ckeditor',
    'settings' => array(
      'buttons' => array(
        'default' => array(
          'Bold' => 1,
          'Italic' => 1,
          'Strike' => 1,
          'BulletedList' => 1,
          'NumberedList' => 1,
          'Outdent' => 1,
          'Indent' => 1,
          'Undo' => 1,
          'Redo' => 1,
          'Link' => 1,
          'Unlink' => 1,
          'Anchor' => 1,
          'Superscript' => 1,
          'Subscript' => 1,
          'Blockquote' => 1,
          'Source' => 1,
          'HorizontalRule' => 1,
          'ShowBlocks' => 1,
          'RemoveFormat' => 1,
          'Styles' => 1,
          'Table' => 1,
          'SelectAll' => 1,
          'Find' => 1,
          'Replace' => 1,
          'Maximize' => 1,
          'Scayt' => 1,
        ),
        'socialmedia' => array(
          'Facebook' => 1,
          'Twitter' => 1,
          'Livestream' => 1,
          'Mailman' => 1,
          'Tableau' => 1,
          'Tint' => 1,
          'Vimeo' => 1,
          'MailChimp' => 1,
          'Hootsuite' => 1,
          'CodePen' => 1,
          'Social Intents' => 1,
        ),
        'embedded_calltoaction' => array(
          'Call to Action' => 1,
        ),
        'embedded_factsfigures' => array(
          'Facts and Figures' => 1,
        ),
        'embedded' => array(
          'Timeline' => 1,
        ),
        'galleryembedded' => array(
          'Imagegallery' => 1,
        ),
        'embeddedMaps' => array(
          'Embedded Maps' => 1,
        ),
        'embedded_powerbi' => array(
          'Power BI' => 1,
        ),
        'uw_video_embed' => array(
          'uw_video_embed' => 1,
        ),
        'smallerselection' => array(
          'smallerselection' => 1,
        ),
        'uw_config' => array(
          'uw_config' => 1,
        ),
        'imagealign' => array(
          'Image Left' => 1,
          'Image Center' => 1,
          'Image Right' => 1,
        ),
        'clearfloat' => array(
          'Clear Floats' => 1,
        ),
        'templates' => array(
          'Templates' => 1,
        ),
        'uw_liststyle' => array(
          'uw_liststyle' => 1,
        ),
        'codemirror' => array(
          'codemirror' => 1,
        ),
        'textselection' => array(
          'textselection' => 1,
        ),
        'balloonpanel' => array(
          'Balloonpanel' => 1,
        ),
        'a11ychecker' => array(
          'A11ychecker' => 1,
        ),
        'drupal_path' => array(
          'Link' => 1,
        ),
        'drupal' => array(
          'break' => 1,
        ),
      ),
      'theme' => 'advanced',
      'language' => 'en',
      'toolbarLocation' => 'top',
      'resize_enabled' => 1,
      'default_toolbar_grouping' => 0,
      'simple_source_formatting' => 1,
      'acf_mode' => 0,
      'acf_allowed_content' => '',
      'css_setting' => 'none',
      'css_theme' => 'uw_adminimal_theme',
      'css_path' => '',
      'stylesSet' => '',
      'block_formats' => 'p,address,pre,h2,h3,h4,h5,h6,div',
      'forcePasteAsPlainText' => 0,
      'pasteFromWordPromptCleanup' => 0,
      'pasteFromWordRemoveFontStyles' => 1,
    ),
    'preferences' => array(
      'default' => 1,
      'user_choose' => 0,
      'show_toggle' => 1,
      'add_to_summaries' => 1,
      'version' => '4.16.1.cae20318d4',
    ),
    'name' => 'formatuw_tf_standard_wide',
    'rdf_mapping' => array(),
  );

  return $profiles;
}
